import { Component, OnInit } from '@angular/core';
import { TodoService } from '../../services/todo.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

    private name:string;
    private age:number;
    private email:string;
    //dictionary
    private address:{
      street:string,
      city:string,
      province:string,
      postcode:string
    }

    private todoList:Todo [];

    //array
    private skills:string[] ;
   
 

  constructor(private todoService: TodoService) { }

  ngOnInit() {
    this.name ="peng";
    this.age = 10;
    this.email="peng@peng.com"

    this.address ={
      street:"1/2",
      city:"Silom",
      province:"Bangkok",
      postcode:"10550"
    } 
    this.skills= ["JAVA","PHP"];

    //call service
    this.todoService.getTodoList().subscribe((response) =>{
      this.todoList = response;
    })   
  }

  addSkill(skill){
    this.skills.unshift(skill);
    return false;
  }

  removeSkill(skill){
    this.skills.forEach((element,index) => {
      if(element == skill){
        this.skills.splice(index,1);
      }
    });
    
    return false;
  }
}

interface Todo {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
}