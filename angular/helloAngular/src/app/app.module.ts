import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { UserComponent } from './components/user/user.component';
import { AboutusComponent } from './components/Aboutus/Aboutus.component';
import { FormsModule } from '@angular/forms';

import { TodoService} from './services/todo.service';
import { HttpModule } from '@angular/http';
 
@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    AboutusComponent,
    HttpModule
   
  ],
  imports: [
    BrowserModule, 
    FormsModule
    
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
