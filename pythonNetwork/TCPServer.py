import socket
def mainRun():
    #host="localhost"
    host="127.0.0.1"
    port=5000
    server=socket.socket()
    server.bind((host,port))
    server.listen(1) #spccify nember of client
    print("Waiting for client : ")
    client,addr=server.accept()
    print("Conect from :"+str(addr))
    # data tranfer procedure
    while True:
        #receive data from client
        data=client.recv(1024).decode('utf-8') # Byte to string
        if not data : 
            break
        print("Message from client :" + data)
        data=str(data.upper())
        client.send(data.encode('utf-8'))

    client.close()

if __name__=="__main__":
    mainRun()